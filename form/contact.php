<?php
session_start();

require '../functions/functions.php';

$error = [];


if (isset($_POST['envoyer'])) {
    if (!empty($_POST['nickname']) and preg_match("/^[\-a-zA-Z0-9_. ]+$/", $_POST['nickname'])) {
        $nickname = $_POST['nickname'];
        if (!empty($_POST['nicknameDiscord'])) {
            $nicknameDiscord = check_input($_POST['nicknameDiscord']);
            if (!empty($_POST['lienTracker']) or preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $_POST['lienTracker'])) {
                $lienTracker = $_POST['lienTracker'];
                if (!empty($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $mail = check_input($_POST['email']);
                    if (!empty($_POST['subject']) and preg_match("/^[a-zA-Z ]+$/", $_POST['subject'])) {
                        $subjectContact = $_POST['subject'];
                        if (!empty($_POST['message'])) {
                            $messageContact = check_input($_POST['message']);

                            $_SESSION['sucess'] = 1;

                            // Plusieurs destinataires
                            $to = 'Nastywars.publicities@gmail.com';

                            // Sujet
                            $subject = $subjectContact;

                            // message
                            $message = '
                            <html>
                             <head>
                              <title>' . $subjectContact . '</title>
                             </head>
                             <body>
                                    <h2>' . $subjectContact . ' by ' . $nickname . '</h2><br>
                                    <p><b>Nickname: </b>' . $nickname . '</p>
                                    <p><b>Discord: </b>' . $nicknameDiscord . '</p>
                                    <p><b>Tracker: </b><a href="' . $lienTracker . '">' . $lienTracker . '</a></p>
                                    <p><b>Message: </b>' . $messageContact . '</p>

                             </body>
                            </html>
                                ';

                            // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
                            $headers[] = 'MIME-Version: 1.0';
                            $headers[] = 'Content-type: text/html; charset=iso-8859-1';

                            // En-têtes additionnels
                            $headers[] = 'To: Nasty <Nastywars.publicities@gmail.com>';
                            $headers[] = 'From: <' . $mail . '>';
                            $headers[] = 'Cc: ' . $mail . '';
                            $headers[] = 'Bcc: ' . $mail . '';

                            // Envoi
                            mail($to, $subject, $message, implode("\r\n", $headers));

                            header('location: ../index.php');
                            exit();

                        } else {
                            $error['message'] = "Vous n'avez pas rentré de message";
                        }
                    } else {
                        $error['subject'] = "Vous n'avez pas rentré de sujet pour le message";
                    }
                } else {
                    $error['email'] = "Vous n'avez pas rentré d'email ou l'email n'est pas valide";
                }
            } else {
                $error['lienTracker'] = "Le lien que vous avez entré n'est pas valide";
            }
        } else {
            $error['nicknameDiscord'] = "Vous n'avez pas rentré votre pseudo discord";
        }
    } else {
        $error['nickname'] = "Vous n'avez pas rentré votre pseudo de jeu";
    }
}


if (!empty($error)) {
    $_SESSION['error'] = $error;
    $_SESSION['infos'] = $_POST;
    header('location: ../index.php');
    exit();
}