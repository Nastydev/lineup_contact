<?php
session_start();


?><!doctype html>
<html lang='fr'> <!-- french version -->
<head>
    <meta charset='UTF-8'>
    <meta content='width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'
          name='viewport'>
    <meta content='ie=edge' http-equiv='X-UA-Compatible'>
    <title>Page contact line-up</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Shadows+Into+Light&display=swap" rel="stylesheet">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' rel='stylesheet'/>

    <link href='bootstrap/bootstrap.css' rel='stylesheet'>
    <link href='css/style.css' rel='stylesheet'>
    <script src="js/bootstrap.js"></script>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-light na-bg">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">DAW</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="#presentation">Présentation</a>
                    <a class="nav-link" href="#profil">Profils</a>
                    <a class="nav-link" href="#contact">Contact</a>
                </div>
            </div>
        </div>
    </nav>
</header>
<main>
    <h1>Line-UP FR (5v5)</h1>


    <section id='profil'> <!-- #souhaits anchor corresponds to the second document section -->
        <h2>Nos joueurs</h2>

        <div class='profil'> <!-- container for the gift images -->
            <a href="https://battlefieldtracker.com/bfv/profile/origin/XaM_Fr/overview" target="_blank">
                <figure>
                    <img alt='Photo' src='img/cadeau.png' id="joueur1" title='Joueur 1'>
                    <figcaption>Joueur 1</figcaption>
                </figure>
            </a>
            <a href="" target="_blank">
                <figure>
                    <img alt='Photo' src='img/cadeau.png' id="joueur2" title='Joueur 2'>
                    <figcaption>Joueur 2</figcaption>
                </figure>
            </a>
            <a href="" target="_blank">
                <figure>
                    <img alt='Photo' src='img/cadeau.png' id="joueur3" title='Joueur 3'>
                    <figcaption>Joueur 3</figcaption>
                </figure>
            </a>
            <a href="" target="_blank">
                <figure>
                    <img alt='Photo' src='img/cadeau.png' id="joueur4" title='Joueur 4'>
                    <figcaption>Joueur 4</figcaption>
                </figure>
            </a>

        </div>
    </section>

    <section id='presentation'>
        <h2>Vous voulez postuler?</h2>
        <div class="block-first-section">
            <p><strong>! Actuellement nous recherchons un joueur pour integrer la line-up Français Battlefield V
                    !</strong></p>
            <div><h3>Prérequis avant de postuler</h3>
                <ul>
                    <li>Parler Français</li>
                    <li>Avoir au moins 18 ans</li>
                    <li>Un ratio suprérieur à 3</li>
                    <li>Un score minute supérieur à 600</li>
                    <li>Un kill minute supérieur à 1,5</li>

                </ul>
                <h4>Armes et classes:</h4>
                <ul>
                    <li>Assaut: Avoir plus de 26% de précision sur la sturmgewer et/ou la M2</li>
                    <li>Medic: Avoir plus de 24% de précision sur la ZK-383</li>
                    <li>Support : Avoir plus de 27% sur la FG42 ou 26% sur la Bar</li>
                </ul>
            </div>
            <p>Toujours interessé? Alors rendez vous sur le formulaire de <a href="#contact">contact</a> et
                peut-être
                serez vous présent à nos coté pour remporter cette troisième coupe!</p>
        </div>
    </section> <!-- end of #ba section -->

    <section id='contact'><!-- #livraison anchor corresponds to the third document section -->
        <h2>Contact</h2>
        <?php
        if (!empty($_SESSION['error'])): ?>
            <div class="block-center">
                <div class="block-message error"><?php echo implode('<br>', $_SESSION['error']); ?></div>
            </div>
        <?php endif; ?>

        <?php
        if (isset($_SESSION['sucess']) == 1): ?>
            <div class="block-center">
                <div class="block-message sucess">
                    <p>Votre message à bien été envoyé!</p>
                </div>
            </div>
        <?php endif; ?>

        <form action="form/contact.php" method="post">
            <?php require 'functions/functions.php' ?>
            <div class="form-contact">

                <label for="nickname">Votre pseudo de jeu:</label><br>
                <input type="text" id="nickname" name="nickname"
                       value="<?php if (isset($_SESSION['infos'])) {
                           echo htmlspecialchars($_SESSION['infos']['nickname']);
                       } ?>"><br>


                <label for="nicknameDiscord">Votre pseudo discord:</label><br><br>

                <input type="text" id="nicknameDiscord" name="nicknameDiscord"
                       value="<?php if (isset($_SESSION['infos'])) {
                           echo htmlspecialchars($_SESSION['infos']['nicknameDiscord']);
                       } ?>"><br>


                <label for="lienTracker">Lien de votre profil <a href="https://battlefieldtracker.com"
                                                                 target="_blank">battlefieldtracker:</a></label><br>

                <input type="url" id="lienTracker" name="lienTracker"
                       value="<?php if (isset($_SESSION['infos'])) {
                           echo htmlspecialchars($_SESSION['infos']['lienTracker']);
                       } ?>"><br>


                <label for="email">Votre adresse adresse email: </label><br>
                <input type="email" id="email" name="email" value="<?php if (isset($_SESSION['infos'])) {
                    echo htmlspecialchars($_SESSION['infos']['email']);
                } ?>"><br>


                <label for="subject">Sujet du message:</label><br>
                <input type="text" id="subject" name="subject"
                       value="<?php if (isset($_SESSION['infos'])) {
                           echo htmlspecialchars($_SESSION['infos']['subject']);
                       } ?>"><br>


                <label for="message">Message:</label><br>
                <textarea id="message" name="message" rows="5"
                          cols="30"><?php if (isset($_SESSION['infos'])) {
                        echo check_input($_SESSION['infos']['message']);
                    } ?></textarea><br>


                <input type="submit" value="Envoyer le message" name="envoyer" class="envoyer"><br>

            </div>
        </form>
    </section><!-- end of #souhaits section -->

</main> <!-- end of the main content of the page -->

<footer> <!-- start of the page footer -->
    <p>2021 &copy; Battefield Scrims PC By Nastywars</p>
</footer><!-- end of the footer -->

</body>
</html>
<?php
unset($_SESSION['infos']);
unset($_SESSION['error']);
unset($_SESSION['message']);
unset($_SESSION['sucess']);
?>