<?php
function check_input($check)
{
    $check = trim($check);
    $check = stripslashes($check);
    $check = htmlspecialchars($check);
    $check = preg_replace("/\s+/", " ", $check);
    return $check;
}